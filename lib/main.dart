import 'package:flutter/material.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.pink,
        scaffoldBackgroundColor: Colors.pink[200],
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Home-หน้าแรก"),
        ),
        body:
        // Center(
        //   child: Text(
        //     "Hello My App",
        //     style: TextStyle(
        //       fontSize: 35,
        //       fontWeight: FontWeight.bold,
        //     ),
        //   ),
        // ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
           // FlutterLogo(size: 50,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircleAvatar(
                      backgroundImage: AssetImage("images/sareena.jpg"),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Sareena" , style: TextStyle (fontSize: 22),),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Chujan", style: TextStyle (fontSize: 22),),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
